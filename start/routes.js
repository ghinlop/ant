'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.on('/').render('index')

Route.group(() => {
    Route.on('/').render('themes/vuta/pages/index')
    Route.on('/about').render('themes/vuta/pages/about')
    Route.on('/about/company').render('themes/vuta/pages/company')
    Route.on('/about/teacher').render('themes/vuta/pages/teacher')
    Route.on('/about/doanhnghiep').render('themes/vuta/pages/doanhnghiep')
    Route.on('/about/doanhnghiep/detail').render('themes/vuta/pages/doanhnghiep-detail')
    Route.on('/event').render('themes/vuta/pages/event')
    Route.on('/news').render('themes/vuta/pages/news')
    Route.on('/news/content').render('themes/vuta/pages/news_content')
}).prefix('vuta')

Route.group(() => {
    Route.on('/').render('themes/pqe/pages/index')
    Route.on('/about').render('themes/pqe/pages/about')
    Route.on('/contact').render('themes/pqe/pages/contact')
    Route.on('/news').render('themes/pqe/pages/news')
    Route.on('/detail').render('themes/pqe/pages/news-detail')
}).prefix('pqe')


Route.group(() => {
    Route.get('/nhanvien', function ({ response}){
        const data = [
            {
                img: "https://pickaface.net/gallery/avatar/demo.webmaster541295de29059.png",
                name: "Mr.Tam",
                pos: "Giam Doc",
                desc: "Nguoi thanh lap cong ty"
            },
            {
                img: "http://demo.powowbox.com/powowbox/avatar_demo/Jane_0001.png",
                name: "Mrs.Duyen",
                pos: "Pho Giam Doc",
                desc: "Nguoi thanh lap cong ty"
            },
            {
                img: "https://pickaface.net/gallery/avatar/unr_demo_181102_1037_17ut.png",
                name: "Mr.Hung",
                pos: "Truong Phong Kinh Doanh",
                desc: "Nguoi thanh lap cong ty"
            },
            {
                img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRkVB_rbSkENJWrn81a4upzqYr6BaHM7ruqHbRVjtFydlTLGRosrQ",
                name: "No Name",
                pos: "Hacker",
                desc: "Undergroup"
            },

        ]
        return response.json(data)
    })
}).prefix('api')