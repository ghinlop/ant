const mix = require('laravel-mix');
const autoprefixer = require('autoprefixer')
const mqpacker = require('css-mqpacker')
const name = 'pqe'
mix.options({
    postCss: [
        autoprefixer({
            browsers: ['last 3 versions', 'iOS >= 8', 'Safari >= 8', 'ie >= 9']
        }),
        mqpacker({
            sort: true
        }),
    ],
    processCssUrls: false
});

mix.sass('resources/scss/' + name + '.scss', 'public/themes/' + name + '/css');
mix.js('resources/js/' + name + '.js', 'public/themes/' + name + '/js')
// mix.copy([
//     'node_modules/bootstrap/dist/css/bootstrap.min.css',
//     'node_modules/bootstrap/dist/css/bootstrap.min.css.map',
//     'node_modules/font-awesome/css/font-awesome.css',
//     'node_modules/font-awesome/css/font-awesome.css.map',
// ], 'public/themes/' + name + '/css')
//     .copy('node_modules/font-awesome/fonts/*', 'public/themes/' + name + '/fonts')
//     .copy([
//         'node_modules/bootstrap/dist/js/bootstrap.min.js',
//         'node_modules/bootstrap/dist/js/bootstrap.min.js.map',
//         'node_modules/jquery/dist/jquery.slim.min.js',
//         'node_modules/jquery/dist/jquery.slim.min.map',
//         'node_modules/popper.js/dist/umd/popper.min.js',
//         'node_modules/popper.js/dist/umd/popper.min.js.map',
//     ], 'public/themes/' + name + '/js')

// mix.webpackConfig({
//     resolve: {
//         alias: {
//             "@": path.resolve(
//                 __dirname,
//                 "resources/assets/js"
//             ),
//             "@sass": path.resolve(
//                 __dirname,
//                 "resources/assets/sass"
//             ),
//         }
//     }
// });

// mix.browserSync('ghin.test:3333')navbar