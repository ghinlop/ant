
$('[move_scroll]').click(function (e) {
    e.preventDefault();
    var TargetMove = $(this).attr('move_scroll');
    var position = document.querySelector(TargetMove).offsetTop;
    var scroll = 0;
    var event_move = setInterval(() => {
        if (scroll > position) return clearInterval(event_move)
        scroll = scroll + 10
        window.scrollTo(0, scroll)
    }, 10);
})
$(document).ready(() => {

    var video_height = $('video').height();
    if (video_height && video_height <= $(window).height()) {
        $('.header-video').height(video_height);
    }

    var titleNewsSecondary = $('.news-secondary ._title');
    for (let title of titleNewsSecondary) {
        if (title.innerText.length >= 60) {
            title.innerText = title.innerHTML.substring(0, 55) + '...'

        }
    }

    $('[modal-toggle]').on('click', (e) => {
        e.preventDefault();
        let content_id = e.target.attributes['modal-toggle'].value;
        let get_html_content = document.querySelectorAll(content_id);

        document.getElementsByClassName('modal-title')[0].innerHTML = get_html_content[0].innerText;
        document.getElementsByClassName('modal-body')[0].innerHTML = get_html_content[1].innerHTML;
        $('.modal').modal('show')
    })

    $('.company-slides').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        centerPadding: '15px',
    });

    var about_slider = document.querySelectorAll('[data-slick]')
    // console.log(about_slider)
    for (let slider of about_slider) {
        $(slider).slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            centerPadding: '20px'
        })
    }

    // if (about_slider) {
    //     // $('[data-img-render] a').click((e) => {
    //     //     e.preventDefault();
    //     //     let img_url = e.target.attributes['src'].value;            
    //     //     let img = new Image();
    //     //     img.src = img_url;
    //     //     let modal_content = document.querySelector('[data-modal-content] .modal-body [content-render]')
    //     //     // modal_content.appendChild(img)
    //     //     // modal_content.querySelector('img').classList.add('w-100')  
    //     //     // $('.modal').modal('show')
    //     // })
    //     // var filter_id = /(\?|&)v=([^&#]+)/;
    //     // for(let ytb of document.querySelectorAll('[data-ytb-url]')){
    //     //     let url = ytb.attributes['data-ytb-url'].value;

    //     //     let _id = url.match(filter_id)[2]
    //     //     console.log(_id)
    //     //     let img = new Image();
    //     //     img.src = `https://img.youtube.com/vi/${_id}/hqdefault.jpg`
    //     //     ytb.appendChild(img);
    //     //     ytb.setAttribute('ytb-id', _id)
    //     // }

    //     // $('[ytb-id]').click((e) => {
    //     //     e.preventDefault();
    //     //     let _id = e.currentTarget.attributes['ytb-id'].value
    //     //     let modal_content = document.querySelector('[data-modal-content] .modal-body [content-render]')
    //     //     modal_content.innerHTML = `
    //     //         <iframe width="100%" height="100%" src="https://www.youtube.com/embed/${_id}?autoplay=1&enablejsapi=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="min-height:300px"></iframe>
    //     //     `
    //     //     $('.modal').modal('show')
    //     // })
        
    // }
    // $('[data-modal-content]').on('hidden.bs.modal', function (e) {
    //     document.querySelector('[data-modal-content] .modal-body [content-render]').innerHTML = '\n'
    // })
    var getToggleBtn = $('[data-menu]');
    var el = $(getToggleBtn).attr('data-menu');
    if (window.innerWidth <= 1023) {
        var navbar = document.querySelector('#navbar').clientHeight
        $(el).attr('style', `height: calc(100vh - ${navbar}px); top: ${navbar}px`)
    }
    // $('.dropdown a').click(function(event){
    //     console.log(event.currentTarget.parentElement)
    // })
    

    $(getToggleBtn).click(function (e) {
        e.preventDefault();
        $(el).toggleClass('active')
        $('body').toggleClass('overflow-hidden')
    })
    // var getSlides = document.querySelectorAll('[data-slider]')
    // if (getSlides.length > 0) {
    //     for (let slide of getSlides) {
    //         let id = slide.attributes['id'].value
    //         $(slide).slick({
    //             slidesToShow: 1,
    //             slidesToScroll: 1,
    //             autoplay: true,
    //             autoplaySpeed: 2000,
    //             centerPadding: '15px',
    //             infinite: true,
    //             arrows: false,
    //             asNavFor: slide.attributes['data-slider'].value
    //         })
    //     }
    // }
    // var getSlides = document.querySelectorAll('[data-slider-content]')
    // if (getSlides.length > 0) {
    //     for (let slide of getSlides) {
    //         $(slide).slick({
    //             slidesToShow: 1,
    //             slidesToScroll: 1,
    //             autoplay: true,
    //             autoplaySpeed: 2000,
    //             centerPadding: '15px',
    //             infinite: true,
    //             arrows: false,
    //             draggable: false,
    //             pauseOnHover: false,
    //             asNavFor: slide.attributes['data-slider-content'].value
    //         })
    //     }
    // }
    // var getSlideThumbs = document.querySelectorAll("[data-thumb]")
    // if (getSlideThumbs.length > 0) {
    //     for (let slide_thumb of getSlideThumbs) {
    //         let id = slide_thumb.attributes['id'].value
    //         $(slide_thumb).slick({
    //             vertical: true,
    //             slidesToShow: 2,
    //             focusOnSelect: true,
    //             infinite: true,
    //             centerMode: true,
    //             arrows: false,
    //             asNavFor: slide_thumb.attributes['data-thumb'].value,
    //             responsive: [
    //                 {
    //                     breakpoint: 767,
    //                     settings: {
    //                         vertical: false,
    //                     }
    //                 }
    //             ]
    //         })
    //     }
    // }

})

$(function () {
    window.onscroll = function () { navbarSticky() };

    var navbar = document.getElementById("navbar");

    navbarSticky();

    function navbarSticky() {
        if (window.innerWidth >= 1204) {
            if (window.pageYOffset >= document.querySelector('[data-sticky]').offsetTop && window.pageYOffset != 0) {
                navbar.classList.add("sticky")
            } else {
                navbar.classList.remove("sticky");
            }
        } else {
            if (window.pageYOffset > 0) {
                navbar.classList.add("sticky")
            } else {
                navbar.classList.remove("sticky");
            }
        }
    }
    var get_breadcrumb = document.querySelector('.breadcrumb-item.active')
    // console.log(get_breadcrumb.innerText)
    if (get_breadcrumb) {
        if (window.innerWidth >= 1024 && get_breadcrumb.innerText.length > 150) {
            document.querySelector('.breadcrumb-item.active').innerText = get_breadcrumb.innerText.substring(0, 140) + '...'
        }
        if (window.innerWidth <= 768 && get_breadcrumb.innerText.length > 70) {
            document.querySelector('.breadcrumb-item.active').innerText = get_breadcrumb.innerText.substring(0, 60) + '...'
        }
    }
})

import Vue from 'vue';
import store from './vuta/store/index'
// import components
import vImage from './vuta/components/Image.vue';
import vModal from './vuta/components/Modal.vue';
import vSlider from './vuta/components/slider.vue';
import vLoading from './vuta/components/Loading.vue';

var app = new Vue({
    components: {
         vImage,
        vModal,
         vSlider,
         vLoading
     },
     store
 }).$mount('#app')
