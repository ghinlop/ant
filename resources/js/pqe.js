$(function () {
    window.onscroll = function () { navbarSticky() };

    var targetSticky = document.querySelector('[data-sticky-target]');

    navbarSticky();

    function navbarSticky() {
        if (window.innerWidth >= 1204) {
            if (window.pageYOffset >= document.querySelector('[data-sticky]').offsetTop && window.pageYOffset != 0) {
                targetSticky.classList.add("sticky")
            } else {
                targetSticky.classList.remove("sticky");
            }
        } else {
            if (window.pageYOffset > 0) {
                targetSticky.classList.add("sticky")
            } else {
                targetSticky.classList.remove("sticky");
            }
        }
    }
})



$(document).ready(function () {
    var getToggleAside = document.querySelector('[data-toggle]');
    var eToggle = getToggleAside.attributes['data-toggle'].value;

    var menu_target = document.querySelector(eToggle);
    var menu_li = menu_target.querySelectorAll("li")
    if (menu_li.length && menu_target){
        var total_height = menu_li[0].clientHeight * menu_li.length;
    }

    getToggleAside.addEventListener('click', function () {
        console.log(total_height)
        if (menu_target.classList[menu_target.classList.length - 1] == "active"){
            menu_target.style.height = 0;
            menu_target.style.borderTopWidth = 0;
        }else{
            menu_target.style.height = total_height+5+"px";
            menu_target.style.borderTopWidth = "1px";
        }
        menu_target.classList.toggle('active');
        console.log(menu_target.classList[menu_target.classList.length - 1] == "active")
    })
    var mySwiper = new Swiper('.swiper-container', {
        // Optional parameters
        // direction: 'vertical',
        slidesPerView: 3,
        spaceBetween: 30,
        loop: true,
        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            1024: {
                slidesPerView: 3,
                spaceBetween: 40,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 30,
            },
            640: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 10,
            }
        }
    })
})




// function resize_aside() {
//     let window_size = window.innerWidth;
//     let height_target = document.querySelector('[target-height]');
//     let aside_id = document.querySelector('[data-toggle]').attributes['data-toggle'].value;
//     if (window_size <= 1023 && height_target.attributes['target-height'].value !== true) {
//         let header = height_target.clientHeight;
//         let aside = document.querySelector(aside_id)
//         $(aside).attr('style', `height: calc(100vh - ${header + 1}px); top: ${header - 1}px`);
//         height_target.attributes['target-height'].value = true
//         console.log(height_target.clientHeight);

//     }
//     if (window_size > 1023 && height_target.attributes['target-height'].value) {
//         let aside = document.querySelector(aside_id)
//         $(aside).removeAttr('style');
//         height_target.attributes['target-height'].value = false
//     }
// }

