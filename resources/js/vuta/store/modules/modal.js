export default {
    namespaced: true,
    state:{
        type: null,
        url: null,
        data: null,
        size: null,
    },
    mutations:{
        setType(state, type){
            state.type = type
        },
        SET_URL(state, url){
            state.url = url
        },
        SET_DATA(state, data){
            state.data = data
        },
        SET_MODAL_SIZE(state, size){
            state.size = size
        }
    },
    actions:{
        changeType({commit}, data){
            commit('setType', data)
        },
        changeURL({commit}, url){
            if(url){
                commit('SET_URL', url)
            }else{
                commit('SET_URL', null)
            }
        },
        changeData({commit}, data){
            if(data){
                commit('SET_DATA', data);
            }
        },
        changeModalSize({commit}, size){
            if(size){
                commit('SET_MODAL_SIZE', size)
            }
        }
    }
}