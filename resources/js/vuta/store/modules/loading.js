import http from 'axios';

export default {
    namespaced: true,
    state: {
        data: null,
        loading: false,
        error: false
    },
    mutations: {
        SET_LOADING(state, data){
            state.loading = data
        },
        SET_data(state, data){
            state.data = data
        },
        SET_ERROR(state, error){
            state.error = error
        }
    },
    actions: {
        async LOAD_DATA({commit}, url){
            commit('SET_LOADING', true)
            await http.get(url)
                .then(res => {
                    commit('SET_data', res.data)
                    commit('SET_LOADING', false)
                })
                .catch(err => {
                    commit('SET_ERROR', err)
                    commit('SET_LOADING', false)
                })
                
        }
    }
}