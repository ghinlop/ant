import Vue from 'vue';
import Vuex from 'vuex'

// import module
import Modal from './modules/modal';
import Loading from './modules/loading'
Vue.use(Vuex);

export default new Vuex.Store({
    strict: true,
    namespaced: false,
    modules:{
        Modal,
        Loading
    }
})